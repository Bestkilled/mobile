import 'dart:convert';

import 'package:flutter/services.dart';
import '../models/books.dart';
import '../models/explore_data.dart';
import '../models/models.dart';

class MockBookstoreService {
  Future<ExploreData> getExploreData() async {
    final mathematics = await _getMath();
    final english = await _getEng();
    final computer = await _getCom();
    return ExploreData(mathematics, english, computer);
  }


  Future<List<Book>> _getMath() async {
    await Future.delayed(const Duration(milliseconds: 300));
    final dataString =
    await _loadAsset('assets/sample_data/book_detail.json');
    // Decode to json
    final Map<String, dynamic> json = jsonDecode(dataString);
    // Go through each post and convert json to Post object.
    if (json['math'] != null) {
      final math = <Book>[];
      json['math'].forEach((v) {
        math.add(Book.fromJson(v));
      });
      return math;
    } else {
      return [];
    }
  }
  Future<List<Book>> _getEng() async {
    await Future.delayed(const Duration(milliseconds: 300));
    final dataString =
    await _loadAsset('assets/sample_data/book_detail.json');
    // Decode to json
    final Map<String, dynamic> json = jsonDecode(dataString);
    // Go through each post and convert json to Post object.
    if (json['english'] != null) {
      final english = <Book>[];
      json['english'].forEach((v) {
        english.add(Book.fromJson(v));
      });
      return english;
    } else {
      return [];
    }
  }
  Future<List<Book>> _getCom() async {
    await Future.delayed(const Duration(milliseconds: 300));
    final dataString =
    await _loadAsset('assets/sample_data/book_detail.json');
    // Decode to json
    final Map<String, dynamic> json = jsonDecode(dataString);
    // Go through each post and convert json to Post object.
    if (json['computer'] != null) {
      final computer = <Book>[];
      json['computer'].forEach((v) {
        computer.add(Book.fromJson(v));
      });
      return computer;
    } else {
      return [];
    }
  }

  // Loads sample json data from file system
  Future<String> _loadAsset(String path) async {
    return rootBundle.loadString(path);
  }
}
