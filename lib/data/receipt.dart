

class Receipt {
  String _purID='';
  String _name = '';
  String _address = '';
  String _tel ='';
  int _amount = 0;
  double _totalPrice = 0;
  dynamic _order;

  Receipt(id, name, add,tel, amount, price, order) {
    _purID =id;
    _name = name;
    _address = add;
    _tel = tel;
    _amount = amount;
    _totalPrice = price;
    _order = order;
  }

  String get purID => _purID;

  set purID(String value) {
    _purID = value;
  }
  String get tel => _tel;

  set tel(String value) {
    _tel = value;
  }
  String get name => _name;

  set name(String value) {
    _name = value;
  }
  String get address => _address;

  set address(String value) {
    _address = value;
  }

  int get amount => _amount;

  set amount(int value) {
    _amount = value;
  }

  double get totalPrice => _totalPrice;

  set totalPrice(double value) {
    _totalPrice = value;
  }

  dynamic get order => _order;

  set order(dynamic value) {
    _order = value;
  }
}

class SelectedItems {
  dynamic listOrder;
  bool check = false;

  SelectedItems(list, check) {
    listOrder = list;
    check = check;
  }

}

List<Receipt> receipt = [];
DateTime dateToday =new DateTime.now();
String date = dateToday.toString().substring(0,10);