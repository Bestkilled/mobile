import 'dart:math';

class Order{
  String _name='';
  String _img='';
  int _amount=0;
  double _price=0;
  double _totalPrice=0;
  bool _check = false;

  Order(name,img,amount,price){
    _name=name;
    _img=img;
    _amount=amount;
    _price=price;
    _totalPrice=amount*price;
    _check = false;
  }
  bool get getCheck => _check;

  void setCheck(bool value) {
    _check = value;
  }
  String get getName => _name;

  set setName(String value) {
    _name = value;
  }
  String get getImg => _img;

  void setImg(String value) {
    _img = value;
  }

  int get getAmount => _amount;

  void setAmount(int value) {
    _amount += value;
  }

  double get getPrice => _price;

  set setPrice(double value) {
    _price = value;
  }

  double get getTotalPrice => _totalPrice;

  void setTotalPrice() {
    _totalPrice = _amount*_price;
  }

}
double total(x) {
  double total = 0;
  for (var i = 0; i < x.length; i++) {
    total += (x[i].getAmount*x[i].getPrice);
    print('-- ='+x[i].getTotalPrice.toString());
  }
  return total;
}
String randomID(){
  Random rnd;
  int min = 10000;
  int max = 99999;
  rnd = Random();
  int r = min + rnd.nextInt(max - min);
  return 'TH'+r.toString();
}