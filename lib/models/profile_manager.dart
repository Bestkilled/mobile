import 'package:flutter/material.dart';


class BookPageManager extends ChangeNotifier {
  bool get didSelectBook => _didSelectBook;
  bool get darkMode => _darkMode;

  var _didSelectBook = false;
  final _darkMode = false;


  void tapOnProfile(bool selected) {
    _didSelectBook = selected;
    notifyListeners();
  }
}
