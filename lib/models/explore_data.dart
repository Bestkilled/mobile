import 'books.dart';

class ExploreData {
  final List<Book> math;
  final List<Book> english;
  final List<Book> computer;

  ExploreData(
      this.math,
      this.english ,
      this.computer
  );
}
