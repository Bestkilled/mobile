class BookPages {
  static String splashPath = '/splash';
  static String loginPath = '/login';
  static String onBoardingPath = '/onboarding';
  static String home = '/';
  static String bookDetail = '/item';
  static String cart = '/cart';
  static String order = '/order';
  static String purchase = '/purchase';
  static String bookList='/booklist';
}
