import 'package:flutter/material.dart';


class PurPageManager extends ChangeNotifier {
  bool get didSelectCheckout => _checkout;
  var _checkout = false;

  void tapOnCheckout(bool x) {
    _checkout = x;
    notifyListeners();
  }

}
