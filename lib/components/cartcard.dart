
import 'package:flutter/material.dart';

import '../data/order.dart';
import '../screens/bookdetail.dart';

class CardOfListPage extends StatefulWidget {
  final Order order;
  final Function() press;

  const CardOfListPage({Key? key, required this.order, required this.press})
      : super(key: key);

  @override
  State<CardOfListPage> createState() => _CardOfListPageState();
}

bool isChecked = false;

class _CardOfListPageState extends State<CardOfListPage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.press,
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
              ),
              width: 30,
              child: Image.asset(widget.order.getImg),
            ),
          ),
          Expanded(
            flex: 6,
            child: Column(
                children:[
                  Text(
                    widget.order.getName,
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                    textAlign: TextAlign.start,
                  ),
                  Text(
                    '  \฿${widget.order.getTotalPrice} ',
                    style: const TextStyle(
                        color: Colors.green, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    '[ \฿${widget.order.getPrice}X${widget.order.getAmount}]',
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w600),
                    textAlign: TextAlign.left,
                  ),
                ]

            ) ,
          ),
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: Colors.grey,
            ),
            child: Checkbox(
              checkColor: Colors.red,
              activeColor: Colors.amberAccent,
              value: isChecked,
              onChanged: (bool? value) {
                setState(() {
                  isChecked = value!;
                  if(isChecked){
                    selectedOrder.add(widget.order);
                  }else if(!isChecked && selectedOrder.isNotEmpty){
                    for(var i = 0; i< selectedOrder.length; i++){
                      if(selectedOrder[i].getName==widget.order.getName){
                        selectedOrder.removeAt(i);
                      }
                    }
                  }
                  print(selectedOrder.length);
                }
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
List<Order> selectedOrder = [];
