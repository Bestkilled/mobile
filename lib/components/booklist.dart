import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/books.dart';
import '../models/models.dart';
import 'bookcard.dart';
List<Book> selectBook =[];
class Mathematics extends StatelessWidget {
  final List<Book> books;

  const Mathematics({
    Key? key,
    required this.books,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16,
        right: 16,
        top: 16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Mathematics books',
            style: Theme
                .of(context)
                .textTheme
                .headline1,
          ),
          const SizedBox(height: 16),
          Container(
            height: 360,
            color: Colors.transparent,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: books.length,
              itemBuilder: (context, index) {
                final book = books[index];
                return BookCard(book: book,press: (){
                  Provider.of<BookPageManager>(context, listen: false)
                      .tapOnProfile(true);
                  selectBook.add(book);
                },
                );
              },
              separatorBuilder: (context, index) {
                return const SizedBox(width: 10);
              },
            ),
          ),
        ],
      ),
    );
  }

}
class English extends StatelessWidget {
  final List<Book> books;

  const English({
    Key? key,
    required this.books,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16,
        right: 16,
        top: 16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'English books',
            style: Theme
                .of(context)
                .textTheme
                .headline1,
          ),
          const SizedBox(height: 16),
          Container(
            height: 360,
            color: Colors.transparent,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: books.length,
              itemBuilder: (context, index) {
                final book = books[index];
                return BookCard(book: book,press: (){
                  Provider.of<BookPageManager>(context, listen: false)
                      .tapOnProfile(true);
                  selectBook.add(book);
                },
                );
              },
              separatorBuilder: (context, index) {
                return const SizedBox(width: 10);
              },
            ),
          ),
        ],
      ),
    );
  }

}
class Computer extends StatelessWidget {
  final List<Book> books;

  const Computer({
    Key? key,
    required this.books,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16,
        right: 16,
        top: 16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Computer books',
            style: Theme
                .of(context)
                .textTheme
                .headline1,
          ),
          const SizedBox(height: 16),
          Container(
            height: 360,
            color: Colors.transparent,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: books.length,
              itemBuilder: (context, index) {
                final book = books[index];
                return BookCard(book: book,press: (){
                  Provider.of<BookPageManager>(context, listen: false)
                      .tapOnProfile(true);
                  selectBook.add(book);
                },);
              },
              separatorBuilder: (context, index) {
                return const SizedBox(width: 10);
              },
            ),
          ),
        ],
      ),
    );
  }

}