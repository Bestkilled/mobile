
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fooderlich/components/purcard.dart';
import 'package:provider/provider.dart';

import '../data/receipt.dart';
import '../models/purchase_manager.dart';
import '../screens/purchases.dart';

class PurListEmpty extends StatelessWidget {
  const PurListEmpty({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Center(
        child: Card(
          child: SizedBox(
            width: 300,
            height: 300,
            child: Column(
              children: const [
                Icon(
                  Icons.article_outlined,
                  size: 190,
                ),
                Text(
                  'No purchase order',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PurListBody extends StatefulWidget {
  const PurListBody({Key? key}) : super(key: key);

  @override
  State<PurListBody> createState() => _PurListBodyState();
}

class _PurListBodyState extends State<PurListBody> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: receipt.length,
        itemBuilder: (BuildContext context, int index) {
          return Dismissible(
            background: Container(
              color: Colors.red,
              child: const Align(
                alignment: Alignment.center,
                 child: Text(
                  'Deleted',
                  textAlign: TextAlign.right,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              )

            ),
            confirmDismiss: (direction) async {
              if (direction == DismissDirection.endToStart ||
                  direction == DismissDirection.startToEnd) {
                print(index);
                print(receipt.length);
                receipt.removeAt(index);
                Provider.of<PurPageManager>(context, listen: false)
                .tapOnCheckout(false);
                Navigator.popUntil(context, ModalRoute.withName('/purchase'));
                return true;
              }
            },
            key: Key(index.toString()),
            child: PurCard(
              receipt: receipt[index],
              press: () {},
            ),
          );
        },
      ),
    );
  }
}
