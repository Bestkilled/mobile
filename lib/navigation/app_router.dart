import 'package:flutter/material.dart';

import '../models/cart_manager.dart';
import '../models/models.dart';
import '../models/purchase_manager.dart';
import '../screens/comfirm_order.dart';
import '../screens/purchasesdetail.dart';
import '../screens/screens.dart';

class AppRouter extends RouterDelegate
    with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  @override
  final GlobalKey<NavigatorState> navigatorKey;

  final AppStateManager appStateManager;
  final BookPageManager bookManager;
  final BookListPageManager bookListManager;
  final CartPageManager cartManager;
  final PurPageManager purPageManager;

  AppRouter({
    required this.appStateManager,
    required this.bookManager,
    required this.bookListManager,
    required this.cartManager,
    required this.purPageManager,

  }) : navigatorKey = GlobalKey<NavigatorState>() {
    appStateManager.addListener(notifyListeners);
    bookManager.addListener(notifyListeners);
    bookListManager.addListener(notifyListeners);
    cartManager.addListener(notifyListeners);
    purPageManager.addListener(notifyListeners);
  }

  @override
  void dispose() {
    appStateManager.removeListener(notifyListeners);
    bookManager.removeListener(notifyListeners);
    bookListManager.removeListener(notifyListeners);
    cartManager.removeListener(notifyListeners);
    purPageManager.removeListener(notifyListeners);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      onPopPage: _handlePopPage,
      pages: [
        if (!appStateManager.isInitialized) SplashScreen.page(),
        if (appStateManager.isInitialized && !appStateManager.isLoggedIn)
          LoginScreen.page(),
        if (appStateManager.isLoggedIn && !appStateManager.isOnBoardingComplete)
          OnBoardingScreen.page(),
        if (appStateManager.isOnBoardingComplete)
          Home.page(appStateManager.getSelectedTab),
        if (bookManager.didSelectBook)
          BookScreen.page(),
        if (cartManager.didSelectCheckout)
          ConfirmOrder.page(),
        if (purPageManager.didSelectCheckout)
          PurDetail.page()
      ],
    );
  }

  bool _handlePopPage(
      Route<dynamic> route,
      result) {
    if (!route.didPop(result)) {
      return false;
    }

    if (route.settings.name == BookPages.onBoardingPath) {
      appStateManager.logout();
    }

    return true;
  }

  @override
  Future<void> setNewRoutePath(configuration) async => null;
}
