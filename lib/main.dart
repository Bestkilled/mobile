import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'book_theme.dart';
import 'models/cart_manager.dart';
import 'models/models.dart';
import 'models/purchase_manager.dart';
import 'navigation/app_router.dart';

void main() {
  runApp(
    const Bookstore(),
  );
}

class Bookstore extends StatefulWidget {
  const Bookstore({Key? key}) : super(key: key);

  @override
  _BookstoreState createState() => _BookstoreState();
}

class _BookstoreState extends State<Bookstore> {
  final _profileManager = BookPageManager();
  final _appStateManager = AppStateManager();
  final _bookListStateManager = BookListPageManager();
  final _cartStateManager = CartPageManager();
  final _purStateManager = PurPageManager();
  late AppRouter _appRouter;

  @override
  void initState() {
    super.initState();
    _appRouter = AppRouter(
      appStateManager: _appStateManager,
      bookManager: _profileManager,
      bookListManager: _bookListStateManager,
      cartManager: _cartStateManager ,
      purPageManager: _purStateManager,

    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => _appStateManager,
        ),
        ChangeNotifierProvider(
          create: (context) => _profileManager,
        ),
        ChangeNotifierProvider(
          create: (context) => _bookListStateManager,
        ),
        ChangeNotifierProvider(
          create: (context) => _cartStateManager,
        ),
        ChangeNotifierProvider(
          create: (context) => _purStateManager,
        )

      ],
      child: Consumer<BookPageManager>(
        builder: (context, profileManager, child) {
          ThemeData theme;
          if (profileManager.darkMode) {
            theme = BookTheme.dark();
          } else {
            theme = BookTheme.light();
          }

          return MaterialApp(
            theme: theme,
            title: 'Bookstore',
            home: Router(
              routerDelegate: _appRouter,
              backButtonDispatcher: RootBackButtonDispatcher(),
            ),
          );
        },
      ),
    );
  }
}
