import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'bookdetail.dart';
import '../components/cartcard.dart';
import '../components/cartlist.dart';
import '../models/cart_manager.dart';
import '../models/models.dart';

class Cart extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: BookPages.cart,
      key: ValueKey(BookPages.cart),
      child:  const Cart(),
    );
  }
  const Cart({Key? key}) : super(key: key);

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  @override
  Widget build(BuildContext context) {
    if(listOrder.isEmpty){
      return
        Stack(
          children: [
            const CartListEmpty(),
          ],
        );
    }else{
      return
        Stack(
          children: [
            const CartListBody(),
            Column(
                children:<Widget>[
                  //first element in column is the transparent offset
                  Container(
                      height: 580,
                    alignment: Alignment.topRight,
                  ),
                  FlatButton(
                    height: 40,
                    minWidth: 380,
                    color: Colors.black,
                    child: const Text('check out', style:
                    TextStyle(color: Colors.white),),
                    onPressed: () {
                      if(!selectedOrder.isEmpty){
                        Provider.of<CartPageManager>(context, listen: false)
                            .tapOnCheckout(true);
                      }
                    },
                  ),
                ]
            )
          ],

        );
    }

  }
}
