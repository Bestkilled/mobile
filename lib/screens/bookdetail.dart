import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../data/order.dart';
import '../models/books.dart';
import '../models/profile_manager.dart';

class Detail extends StatelessWidget {
  final Book book;

  const Detail({Key? key, required this.book}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          DesImage(
            book: book,
          ),
          Author(
            book: book,
          ),
          Description(book: book),
          AddToCart(
            book1: book,
          ),
        ],
      ),
    );
  }
}

class Description extends StatelessWidget {
  const Description({
    Key? key,
    required this.book,
  }) : super(key: key);

  final Book book;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(27, 7, 18, 5),
        child: Column(
          children: [
            Text(
              book.des,
              style: const TextStyle(fontSize: 16),
            ),
          ],
        ));
  }
}

class AddToCart extends StatefulWidget {
  const AddToCart({
    Key? key,
    required this.book1,
  }) : super(key: key);

  final Book book1;

  @override
  State<AddToCart> createState() => _AddToCartState();
}

class _AddToCartState extends State<AddToCart> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(23, 7, 18, 5),
      child: Row(
        children: <Widget>[
          const CartCounter(),
          const Text(
            'data',
            style: TextStyle(color: Colors.white),
          ),
          Expanded(
            child: SizedBox(
              height: 50,
              child: ButtonTheme(
                buttonColor: Colors.black,
                child: ElevatedButton(
                  onPressed: () {
                    final order = Order(widget.book1.name, widget.book1.img, x,
                        widget.book1.price);
                    var c = true;
                    for (var i = 0; i < listOrder.length; i++) {
                      if (listOrder[i].getName == order.getName) {
                        listOrder[i].setAmount(order.getAmount);
                        listOrder[i].setTotalPrice();
                        c = false;
                        break;
                      }
                    }
                    if (c == true) {
                      listOrder.add(order);
                    }
                    Provider.of<BookPageManager>(context, listen: false)
                        .tapOnProfile(false);
                    Navigator.pop(context);
                    x = 1;
                  },
                  child: Text(
                    'Add to cart'.toUpperCase(),
                    style: const TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

List<Order> listOrder = [];

class Author extends StatelessWidget {
  const Author({
    Key? key,
    required this.book,
  }) : super(key: key);

  final Book book;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(27, 10, 18, 0),
          child: RichText(
            text: TextSpan(
              style: const TextStyle(color: Colors.black),
              children: [
                const TextSpan(text: 'Author\n'),
                TextSpan(
                    text: book.writer,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.bold))
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class DesImage extends StatelessWidget {
  const DesImage({
    Key? key,
    required this.book,
  }) : super(key: key);

  final Book book;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Text(
            ' ',
          ),
          Text(
            book.name,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const SizedBox(height: 25),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Image.asset(
                    book.img,
                    height: 210,
                    width: 140,
                  ),
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    const TextSpan(text: 'Price\n'),
                    TextSpan(
                      text: '\฿${book.price}',
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                          color: Colors.green, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 20),
            ],
          )
        ],
      ),
    );
  }
}

int x = 1;

class CartCounter extends StatefulWidget {
  const CartCounter({Key? key}) : super(key: key);

  @override
  _CartCounterState createState() => _CartCounterState();
}

class _CartCounterState extends State<CartCounter> {
  int numOfItems = 1;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        buildOutlineButton(
          icon: Icons.remove,
          press: () {
            if (numOfItems > 1) {
              setState(() {
                numOfItems--;
                x = numOfItems;
              });
            }
          },
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Text(
            // if our item is less  then 10 then  it shows 01 02 like that
            numOfItems.toString().padLeft(2, '0'),
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        buildOutlineButton(
            icon: Icons.add,
            press: () {
              setState(() {
                numOfItems++;
                x = numOfItems;
              });
            }),
      ],
    );
  }

  SizedBox buildOutlineButton(
      {required IconData icon, required Function() press}) {
    return SizedBox(
      width: 40,
      height: 32,
      child: RaisedButton(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18),
        ),
        onPressed: press,
        child: Icon(icon),
      ),
    );
  }
}
