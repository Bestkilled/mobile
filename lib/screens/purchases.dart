
import 'package:flutter/material.dart';

import '../components/purlist.dart';
import '../data/receipt.dart';
import '../models/book_pages.dart';

class Purchases extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: BookPages.purchase,
      key: ValueKey(BookPages.purchase),
      child:  const Purchases(),
    );
  }
  const Purchases({Key? key}) : super(key: key);

  @override
  State<Purchases> createState() => _PurchasesState();
}

class _PurchasesState extends State<Purchases> {
  @override
  Widget build(BuildContext context) {
    if (receipt.isEmpty) {
      return const Scaffold(
            backgroundColor: Colors.white70,
            body: PurListEmpty(),
      );
    } else {
      return const Scaffold(
            backgroundColor: Colors.white70,
            body: PurListBody(),
      );
    }
  }
}
