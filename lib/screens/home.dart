import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../components/cartcard.dart';
import '../models/models.dart';
import 'explore_screen.dart';
import 'cart_screen.dart';
import 'purchases.dart';

class Home extends StatefulWidget {
  static MaterialPage page(int currentTab) {
    return MaterialPage(
      name: BookPages.home,
      key: ValueKey(BookPages.home),
      child: Home(
        currentTab: currentTab,
      ),
    );
  }

  const Home({
    Key? key,
    required this.currentTab,
  }) : super(key: key);

  final int currentTab;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  static List<Widget> pages = <Widget>[
    ExploreScreen(),
    const Cart(),
    const Purchases()

  ];

  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateManager>(
      builder: (
        context,
        appStateManager,
        child,
      ) {
        return Scaffold(
          appBar:
          AppBar(
            title:Text(
              'Book Store',
              style: Theme.of(context).textTheme.headline6,
            ),
            actions: [
              // profileButton(),
            ],
          ),
          body: IndexedStack(
            index: widget.currentTab,
            children: pages,
          ),
          bottomNavigationBar: BottomNavigationBar(
            selectedItemColor:Colors.black,
            unselectedItemColor: Colors.grey,
            currentIndex: widget.currentTab,
            onTap: (index) {
              isChecked=false;
              setState(() {
                selectedOrder = [];
                isChecked=false;
              });
              Provider.of<AppStateManager>(context, listen: false)
                  .goToTab(index);
              Navigator.pop(context);
            },
            items: <BottomNavigationBarItem>[
              const BottomNavigationBarItem(
                icon: Icon(Icons.storefront),
                label: 'Categories',
              ),
              const BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart_rounded),
                label: 'Shopping cart',
              ),
              const BottomNavigationBarItem(
                icon: Icon(Icons.list),
                label: 'Purchases',
              ),
            ],
            iconSize:30,
          ),
        );
      },
    );
  }
}
